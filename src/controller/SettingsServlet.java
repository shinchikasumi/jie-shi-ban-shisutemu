package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import service.UserService;


@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	List<User> users = new UserService().getUser();
        request.setAttribute("users", users);

        User user = (User) request.getSession().getAttribute("loginUser");

        List<String> messages = new ArrayList<String>();
        HttpSession session = request.getSession();

        if(user == null) {
    		session.setAttribute("errorMessages", messages);
   		 	messages.add("ログインしてください。");
   		 	response.sendRedirect("login");
        }else {
        	int position = user.getPosition();
        	if(position == 1) {
        		request.getRequestDispatcher("/settings.jsp").forward(request, response);
        	}else {
        		session.setAttribute("errorMessages", messages);
   		 		messages.add("閲覧権限がありません。");
   		 		response.sendRedirect("./");
        	}
    	}
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    		int id = Integer.parseInt(request.getParameter("id"));
    		UserService userService = new UserService();
    		User user = userService.getUser(id);
    		HttpSession session = request.getSession();
    		session.setAttribute("editUser", user);

    		request.getRequestDispatcher("/edit.jsp").forward(request, response);
    }

}


