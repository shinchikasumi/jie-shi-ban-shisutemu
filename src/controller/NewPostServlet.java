package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.User;
import service.MessageService;

@WebServlet(urlPatterns = { "/newpost" })
public class NewPostServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        User user = (User) request.getSession().getAttribute("loginUser");

        List<String> messages = new ArrayList<String>();
        HttpSession session = request.getSession();

        if(user == null) {
    		session.setAttribute("errorMessages", messages);
   		 	messages.add("ログインしてください。");
   		 	response.sendRedirect("login");
        }else {
        	request.setAttribute("loginUser", user);
        	request.getRequestDispatcher("/newpost.jsp").forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> messages = new ArrayList<String>();

        if (isValid(request, messages) == true) {

            User user = (User) session.getAttribute("loginUser");

            Message message = new Message();
            message.setTitle(request.getParameter("title"));
            message.setText(request.getParameter("text"));
            message.setCategory(request.getParameter("category"));
            message.setUserId(user.getId());

            new MessageService().register(message);

            response.sendRedirect("./");
        } else {
            String text = request.getParameter("text");
            String title = request.getParameter("title");
            String category = request.getParameter("category");

            session.setAttribute("errorMessages", messages);
            request.setAttribute("errorTitle", title);
            request.setAttribute("errorText", text);
            request.setAttribute("errorCategory", category);

            request.getRequestDispatcher("newpost.jsp").forward(request, response);
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
        String text = request.getParameter("text");
        String title = request.getParameter("title");
        String category = request.getParameter("category");

        if (StringUtils.isEmpty(text) == true) {
            messages.add("本文を入力してください。");
        }else if (1000 < text.length()) {
            messages.add("本文は1000文字以下で入力してください。");
        }
        if (StringUtils.isEmpty(title) == true) {
            messages.add("件名を入力してください。");
        }else if (30 < title.length()) {
            messages.add("件名は30文字以下で入力してください。");
        }
        if (StringUtils.isEmpty(category) == true) {
            messages.add("カテゴリを入力してください。");
        }else if (10 < category.length()) {
            messages.add("カテゴリは10文字以下で入力してください。");
        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}
