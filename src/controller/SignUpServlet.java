package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/signup"})
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        User user = (User) request.getSession().getAttribute("loginUser");
        List<String> messages = new ArrayList<String>();
        HttpSession session = request.getSession();

        if(user == null) {
    		session.setAttribute("errorMessages", messages);
   		 	messages.add("ログインしてください。");
   		 	response.sendRedirect("login");
        }else {
        	int position = user.getPosition();
        	if (position == 1) {
        		request.getRequestDispatcher("/signup.jsp").forward(request, response);
        	}else {
        		session.setAttribute("errorMessages", messages);
   		 		messages.add("閲覧権限がありません。");
   		 		response.sendRedirect("./");
        	}
        }
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> messages = new ArrayList<String>();

        HttpSession session = request.getSession();

        if (isValid(request, messages) == true) {
            User user = new User();
            user.setUserName(request.getParameter("userName"));
            user.setAccount(request.getParameter("account"));
            user.setPassword(request.getParameter("password"));
            user.setOfficeName(Integer.parseInt(request.getParameter("officeName")));
            user.setPosition(Integer.parseInt(request.getParameter("position")));

            new UserService().register(user);

            response.sendRedirect("./");
        } else {
            String userName = request.getParameter("userName");
        	String account = request.getParameter("account");
            String password = request.getParameter("password");

            session.setAttribute("errorMessages", messages);
            request.setAttribute("errorUserName", userName);
            request.setAttribute("errorAccount", account);
            request.setAttribute("errorPassword", password);
            request.getRequestDispatcher("signup.jsp").forward(request, response);
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
    	String userName = request.getParameter("userName");
    	String account = request.getParameter("account");
        String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
        String officeName = request.getParameter("officeName");
        String position = request.getParameter("position");

        UserService userService = new UserService();
        User users = userService.getUser(account);

        if(users != null) {
        	messages.add("このログインIDは既に使用されています。");
        }
        if (StringUtils.isEmpty(account) == true) {
            messages.add("ログインIDを入力してください。");
        }else if (!(account.matches("^[0-9a-zA-Z]+$"))) {
            messages.add("ログインIDは半角英数字で入力してください。");
            //IDは半角英数字に限る。
        }else if (StringUtils.length(account) < 6 || StringUtils.length(account) > 20 ) {
            messages.add("ログインIDは6～20文字で入力してください。");
            //IDは6～20文字に限る。
        }
		if (StringUtils.equals(password, password2) == false) {
			messages.add("入力したパスワードと確認用パスワードが一致しません。");
		}
        if (StringUtils.isEmpty(password) == true) {
            messages.add("パスワードを入力してください。");
        }else if(!(password.matches("^[0-9a-zA-Z-/:-@\\[-\\`\\{-\\~]+$"))) {
            messages.add("パスワードは半角文字で入力してください。");
            //IDは半角英数字記号に限る。
        }else if(StringUtils.length(password) < 6 || StringUtils.length(password) > 20 ) {
            messages.add("パスワードは6～20文字で入力してください。");
            //半角6～20文字
        }
        if (StringUtils.isEmpty(userName) == true) {
            messages.add("ユーザー名を入力してください。");
        }else if(StringUtils.length(userName) >= 11) {
        	messages.add("ユーザー名は10文字以下で入力してください。");
        }
        if (StringUtils.equals(officeName, "0") == true) {
        	messages.add("支店を選択してください。");
        }
        if (StringUtils.equals(position, "0") == true) {
        	messages.add("部署・役職を選択してください。");
        }
        if ((StringUtils.equals(officeName, "1")) && (StringUtils.equals(position, "3"))) {
        	messages.add("支店と部署・役職の組み合わせが不正です。");
        }else if ((StringUtils.equals(officeName, "1")) && (StringUtils.equals(position, "4"))) {
        	messages.add("支店と部署・役職の組み合わせが不正です。");
        }else if(!(StringUtils.equals(officeName, "1")) && (StringUtils.equals(position, "1"))) {
        	messages.add("支店と部署・役職の組み合わせが不正です。");
        }else if(!(StringUtils.equals(officeName, "1")) && (StringUtils.equals(position, "2"))) {
        	messages.add("支店と部署・役職の組み合わせが不正です。");
        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}