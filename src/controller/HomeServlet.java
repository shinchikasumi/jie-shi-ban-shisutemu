package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import beans.UserComment;
import beans.UserMessage;
import service.CommentService;
import service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        User user = (User) request.getSession().getAttribute("loginUser");

        boolean isShowMessageForm;
        if (user != null) {
            isShowMessageForm = true;

        } else {
            isShowMessageForm = false;
        }

        List<UserMessage> messages = new MessageService().getMessage((request.getParameter("search")),(request.getParameter("date")),(request.getParameter("last_date")));


        request.setAttribute("loginUser", user);
        request.setAttribute("messages", messages);
        request.setAttribute("isShowMessageForm", isShowMessageForm);

        List<UserComment> comments = new CommentService().getComment();

        boolean isShowCommentForm;
        if (comments != null) {
            isShowCommentForm = true;
        } else {
            isShowCommentForm = false;
        }

        request.setAttribute("userComment", comments);
        request.setAttribute("isShowCommentForm", isShowCommentForm);

        request.getRequestDispatcher("/home.jsp").forward(request, response);
    }

}
