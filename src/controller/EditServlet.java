package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.UserService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        User user = (User) request.getSession().getAttribute("loginUser");

        List<String> messages = new ArrayList<String>();
        HttpSession session = request.getSession();

        if(user == null) {
    		session.setAttribute("errorMessages", messages);
   		 	messages.add("ログインしてください。");
   		 	response.sendRedirect("login");
        }else {
        	int position = user.getPosition();
        	if(position == 1) {
        		request.getRequestDispatcher("/edit.jsp").forward(request, response);
        	}else {
        		session.setAttribute("errorMessages", messages);
   		 		messages.add("閲覧権限がありません。");
   		 		response.sendRedirect("./");
        	}
    	}
    }

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		User editUser = getEditUser(request);

		if (isValid(request, messages) == true) {

			try {

				String password = request.getParameter("password");
				UserService userService = new UserService();
			    User thisUser = userService.getUser(editUser.getId());

				if (StringUtils.isEmpty(password)) {
					password = thisUser.getPassword();
					editUser.setPassword(password);
					new UserService().passUpdate(editUser);
				} else {
					editUser.setPassword(password);
					new UserService().update(editUser);
				}
			} catch (NoRowsUpdatedRuntimeException e) {
				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
				session.setAttribute("errorMessages", messages);
				request.setAttribute("editUser", editUser);
				request.getRequestDispatcher("/edit.jsp").forward(request, response);
				return;
			}
			session.setAttribute("editUser", editUser);
			response.sendRedirect("settings");
		} else {
            String userName = request.getParameter("userName");
        	String account = request.getParameter("account");
            String password = request.getParameter("password");

			session.setAttribute("errorMessages", messages);
            request.setAttribute("errorUserName", userName);
            request.setAttribute("errorAccount", account);
            request.setAttribute("errorPassword", password);
			request.setAttribute("editUser", editUser);
			request.getRequestDispatcher("/edit.jsp").forward(request, response);
		}
	}

	private User getEditUser(HttpServletRequest request)
			throws IOException, ServletException {

		User editUser = new User();
		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setUserName(request.getParameter("userName"));
		editUser.setAccount(request.getParameter("account"));
		editUser.setOfficeName(Integer.parseInt(request.getParameter("officeName")));
		editUser.setPosition(Integer.parseInt(request.getParameter("position")));
		return editUser;
		}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String userName = request.getParameter("userName");
		String account = request.getParameter("account");
		String officeName = request.getParameter("officeName");
		String position = request.getParameter("position");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		int id = Integer.parseInt(request.getParameter("id"));


		UserService userService = new UserService();
	    User user = userService.getUser(account);

	    User thisUser = userService.getUser(id);
	    String originalAccount = thisUser.getAccount();

        if (user != null && (!(StringUtils.equals (account, originalAccount)))) {
        	messages.add("このログインIDは既に使用されています。");
		}
		if (StringUtils.isEmpty(account) == true) {
			messages.add("ログインIDを入力してください。");
		}else if (!(account.matches("^[0-9a-zA-Z]+$"))) {
            messages.add("ログインIDは半角英数字で入力してください。");
            //IDは半角英数字に限る。
        }else if (StringUtils.length(account) < 6 || StringUtils.length(account) > 20 ) {
            messages.add("ログインIDは6～20文字で入力してください。");
        }
        if (!(StringUtils.isEmpty(password)) && StringUtils.length(password) < 6 || StringUtils.length(password) > 20 ) {
            messages.add("パスワードは6～20文字で入力してください。");
            //半角6～20文字
        }
		if (StringUtils.equals(password, password2) == false) {
			messages.add("入力したパスワードと確認用パスワードが一致しません。");
		}
		if (StringUtils.isEmpty(userName) == true) {
			messages.add("ユーザー名を入力してください");
		}else if (StringUtils.length(userName) >= 10 ) {
            messages.add("ユーザー名は10文字以内で入力してください。");
        }
		if ((StringUtils.equals(officeName, "1")) && (StringUtils.equals(position, "3"))) {
        	messages.add("支店と部署・役職の組み合わせが不正です。");
        }else if ((StringUtils.equals(officeName, "1")) && (StringUtils.equals(position, "4"))) {
        	messages.add("支店と部署・役職の組み合わせが不正です。");
        }else if(!(StringUtils.equals(officeName, "1")) && (StringUtils.equals(position, "1"))) {
        	messages.add("支店と部署・役職の組み合わせが不正です。");
        }else if(!(StringUtils.equals(officeName, "1")) && (StringUtils.equals(position, "2"))) {
        	messages.add("支店と部署・役職の組み合わせが不正です。");
        }
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}