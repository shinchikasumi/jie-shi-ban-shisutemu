package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Comment;
import service.CommentService;

@WebServlet(urlPatterns = { "/delete" })
public class IsDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		Comment deleteComment = getDeleteComment(request);
			new CommentService().update(deleteComment);
			session.setAttribute("deleteComment", deleteComment);

			response.sendRedirect("./");
	}


	private Comment getDeleteComment(HttpServletRequest request)
			throws IOException, ServletException {

		Comment deleteComment = new Comment();
		deleteComment.setId(Integer.parseInt(request.getParameter("id")));
		deleteComment.setIsDelete(Integer.parseInt(request.getParameter("isDelete")));
		return deleteComment;
	}


}