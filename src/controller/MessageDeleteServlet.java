package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Message;
import service.MessageService;

@WebServlet(urlPatterns = { "/messagedelete" })
public class MessageDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		Message deleteMessage = getDeleteMessage(request);
			new MessageService().update(deleteMessage);
			session.setAttribute("deleteMessage", deleteMessage);

			response.sendRedirect("./");

    }

	private Message getDeleteMessage(HttpServletRequest request)
			throws IOException, ServletException {

		Message deleteMessage = new Message();
		deleteMessage.setId(Integer.parseInt(request.getParameter("id")));
		deleteMessage.setIsDelete(Integer.parseInt(request.getParameter("isDelete")));
		return deleteMessage;
	}
}