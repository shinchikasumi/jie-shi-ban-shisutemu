package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.LoginService;

@WebServlet(urlPatterns = { "/login"})
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	 HttpSession session = request.getSession();
         String account = request.getParameter("account");
         String password = request.getParameter("password");

         LoginService loginService = new LoginService();
         User user = loginService.login(account, password);

         if(user != null) {
        	 int stopId = user.getStopId();
         		if(stopId != 1) {
         			session.setAttribute("loginUser", user);
         			response.sendRedirect("./");
         		}else {
         			List<String> message = new ArrayList<String>();
	                message.add("ログインIDまたはパスワードが誤っています。");
	                session.setAttribute("errorMessages", message);
	                response.sendRedirect("login.jsp");
         		}
        }else if(StringUtils.isEmpty(account) || StringUtils.isEmpty(password)){
 			List<String> message = new ArrayList<String>();
            message.add("ログインIDまたはパスワードが入力されていません。");
            session.setAttribute("errorMessages", message);
            response.sendRedirect("login.jsp");
        }else {
            List<String> message = new ArrayList<String>();
            message.add("ログインIDまたはパスワードが誤っています。");
            session.setAttribute("errorMessages", message);
            request.setAttribute("errorAccount", account);
            request.setAttribute("errorPassword", password);
            request.getRequestDispatcher("login.jsp").forward(request, response);
        }
    }
}