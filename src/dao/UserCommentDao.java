package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserComment;
import exception.SQLRuntimeException;

public class UserCommentDao {

    public List<UserComment> getUserComments(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("comments.id as id, ");
            sql.append("comments.message_id as message_id, ");
            sql.append("comments.user_id as user_id, ");
            sql.append("comments.is_delete as is_delete, ");
            sql.append("comments.comment as comment, ");
            sql.append("users.user_name as user_name, ");
            sql.append("users.account as account, ");
            sql.append("comments.created_date as created_date ");
            sql.append("FROM comments ");
            sql.append("INNER JOIN users ");
            sql.append("ON comments.user_id = users.id ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserComment> ret = toUserCommentList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserComment> toUserCommentList(ResultSet rs)
            throws SQLException {

        List<UserComment> ret = new ArrayList<UserComment>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                int userId = rs.getInt("user_id");
                int messageId = rs.getInt("message_id");
                int isDelete = rs.getInt("is_delete");
                String comment = rs.getString("comment");
                String account = rs.getString("account");
                String userName = rs.getString("user_name");
                Timestamp createdDate = rs.getTimestamp("created_date");

                UserComment comments = new UserComment();
                comments.setId(id);
                comments.setUserId(userId);
                comments.setMessageId(messageId);
                comments.setIsDelete(isDelete);
                comments.setComment(comment);
                comments.setCreated_date(createdDate);
                comments.setUserName(userName);
                comments.setAccount(account);

                ret.add(comments);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}