package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.UserMessage;
import exception.SQLRuntimeException;

public class UserMessageDao {

    public List<UserMessage> getUserMessages(Connection connection, int num , String search, String date, String last_date) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("messages.id as id, ");
            sql.append("messages.title as title, ");
            sql.append("messages.text as text, ");
            sql.append("messages.comment as comment, ");
            sql.append("messages.category as category, ");
            sql.append("messages.is_delete as is_delete, ");
            sql.append("messages.user_id as user_id, ");
            sql.append("users.account as account, ");
            sql.append("users.user_name as user_name, ");
            sql.append("messages.created_date as created_date ");
            sql.append("FROM messages ");
            sql.append("INNER JOIN users ");
            sql.append("ON messages.user_id = users.id ");
            if(StringUtils.isEmpty(date)) {
            	date="2019/01/01";
            }
            if(StringUtils.isEmpty(last_date)) {
            	last_date="2030/01/01";
            }
            sql.append("WHERE messages.created_date BETWEEN" + "'" + date + "'AND" + "'" + last_date + "'");

            if(!(StringUtils.isEmpty(search))) {
            sql.append("and messages.category like"+ "'%" + search +"%'" );
            }
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserMessage> ret = toUserMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserMessage> toUserMessageList(ResultSet rs)
            throws SQLException {

        List<UserMessage> ret = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                int userId = rs.getInt("user_id");
                int isDelete = rs.getInt("is_delete");
                String account = rs.getString("account");
                String userName = rs.getString("user_name");
                String title = rs.getString("title");
                String text = rs.getString("text");
                String category = rs.getString("category");
                String comment = rs.getString("comment");
                Timestamp createdDate = rs.getTimestamp("created_date");

                UserMessage message = new UserMessage();
                message.setId(id);
                message.setUserId(userId);
                message.setIsDelete(isDelete);
                message.setAccount(account);
                message.setUserName(userName);
                message.setTitle(title);
                message.setText(text);
                message.setCategory(category);
                message.setComment(comment);
                message.setCreated_date(createdDate);

                ret.add(message);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
}