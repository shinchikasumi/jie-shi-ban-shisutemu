<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    	<title>ユーザー登録</title>
    	<link href="./css/style.css" rel="stylesheet" type="text/css">
    	<script>
		function clickEvent() {
			alert("閲覧権限がありません");
		}
		</script>
    </head>
    <body>
        <div class="main-contents">
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>

            <div class="header">
            	<c:if test="${ empty loginUser }">
					<a href="login">ログイン</a>
				</c:if>
				<c:if test="${ not empty loginUser }">
					<a href="./">ホーム</a>
					<a href="newpost">新規投稿</a>
					<c:choose>
					<c:when test="${ loginUser.position == 1 && loginUser.officeName == 1 }" >
					<a href="settings">ユーザー管理</a>
					</c:when>
					<c:when test="${ loginUser.position != 1 || loginUser.officeName != 1 }" >
					<a href="./" onclick="clickEvent()">ユーザー管理</a>
					</c:when>
					</c:choose>
					<a href="logout">ログアウト</a>
				</c:if>
			</div>

			<c:if test="${loginUser.position == 1}">
                <form action="signup" method="post">
	            <br /> <label>【新規ユーザー登録】</label> <br />
                <br /><label for="account">ログインID</label> <input name="account" id="account" value ="${errorAccount}"/> <br />
                <label for="password">パスワード</label> <input name="password" type="password" id="password" value ="${errorPassword}"/> <br />
				<label for="password">パスワード【確認】</label> <input name="password2" type="password" id="password2"/> <br />
				<label for="userName">ユーザー名</label> <input name="userName" id="userName" value ="${errorUserName}" /> <br />

                <label for="officeName">支店</label>
                <select name="officeName">
                <option value="0">選択してください</option>
                <option value="1">001:本社</option>
                <option value="2">002:支店A</option>
                <option value="3">003:支店B</option>
                <option value="4">004:支店C</option>
                </select> <br />

                <label for="position">部署・役職</label>
                <select name="position">
                <option value="0">選択してください</option>
                <option value="1">01:総務人事</option>
                <option value="2">02:情報管理</option>
                <option value="3">03:支店長</option>
                <option value="4">04:社員</option>
                </select> <br />
				<br />
				<c:if test="${ loginUser.position == 1 && loginUser.officeName == 1 }">
                <input type="submit" value="登録" /> </c:if> <br />

                <a href="javascript:history.back()">戻る</a>
            </form>
            </c:if>
            <c:if test="${loginUser.position != 1}">
				<font color="red">
				<c:out value="閲覧権限がありません"/>
				</font>
			</c:if>
			<div class="copylight"> Copyright(c)ShinchiKasumi</div>
        </div>
    </body>
</html>
