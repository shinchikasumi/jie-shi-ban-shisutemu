<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>【ユーザー管理】</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">

		<script type="text/javascript">
		function submitbtn() {
		// 「OK」ボタン押下時
		if(window.confirm('実行しますか？')) {
		alert('OK');
		return true;
		}
		// 「キャンセル」ボタン押下時
		else {
		alert('キャンセルしました');
		return false;
		}
		}
		</script>
		<script>
		function clickEvent() {
			alert("閲覧権限がありません");
			location.href="./";
		}
		</script>
		<script>
		function accessEvent() {
			alert("閲覧権限がありません");
			location.href="./";
		}
		</script>

	</head>
	<body>
		<div class="main-contents">

			<div class="header">
				<c:if test="${ empty loginUser }">
					<a href="login">ログイン</a>
				</c:if>
				<c:if test="${ not empty loginUser }">
					<a href="./">ホーム</a>
					<a href="newpost">新規投稿</a>
					<c:choose>
					<c:when test="${loginUser.position == 1}" >
					<a href="signup">ユーザー登録</a>
					</c:when>
					<c:when test="${loginUser.position != 1}" >
					<a href="./" onclick="clickEvent()">ユーザー登録</a>
					</c:when>
					</c:choose>
					<a href="logout">ログアウト</a>
				</c:if>
			</div>

		<c:if test="${loginUser.position == 1}">
			<div class ="users">
				<br /> <label>【ユーザー管理】</label> <br /><br />
			         <div class ="stopUser">
			            <table border="1" align="left" >
						 <tr><th>管理</th></tr>
			            	<c:forEach items="${users}" var="user" >
			               		 <form action="stop" method="post" onsubmit="return submitbtn();" >
			                    	<tr>
				                    <td>
			                			<c:if test="${user.stopId == 0}">
											<input name="stopId" value="1" id="stop_id" type="hidden"/>
											<input name="id" value="${user.id}" id="id" type="hidden" />
				                			<input type="submit" value="停止" />
				           	 			</c:if>
										<c:if test="${user.stopId == 1}">
											<input name="stopId" value="0" id="stop_id" type="hidden"/>
											<input name="id" value="${user.id}" id="id" type="hidden"/>
											<input type="submit" value="復活" />
										</c:if>
				                    </td>
			                   		</tr>
			                   	</form>
							</c:forEach>
						</table>
					</div>

			            <div class="userList">
			            	<table border="1">
						    <tr><th>編集</th><th>ユーザー名</th><th>ログインID</th><th>支店名</th><th>部署・役職</th></tr>
			               	<c:forEach items="${users}" var="user">
			                	<form action="settings" method="post">
									<tr>
				                    	<td>
				                    	<c:choose>
										<c:when test="${loginUser.position == 1}" >
				                    	<button type="submit" name="id" value="${user.id}" >編集</button></td>
			                        	</c:when>
			                        	<c:when test="${loginUser.position != 1}" >
										<input type="button" onclick="clickEvent()" value="編集">
										</c:when>
										</c:choose>
			                        	<td><c:out value="${user.userName}" /></td>
			                        	<td><c:out value="${user.account}" /></td>
			                        	<td>
			                        	<c:choose>
											<c:when test="${user.officeName == '1' }" >
												<c:out value="001:本社" />
											</c:when>
											<c:when test="${user.officeName == '2' }" >
												<c:out value="002:支店A" />
											</c:when>
											<c:when test="${user.officeName == '3' }" >
												<c:out value="003:支店B" />
											</c:when>
											<c:when test="${user.officeName == '4' }" >
												<c:out value="004:支店C" />
											</c:when>
										</c:choose>
										</td>
			                        	<td>
			                        	<c:choose>
											<c:when test="${user.position == '1' }" >
												<c:out value="01:総務人事" />
											</c:when>
											<c:when test="${user.position == '2' }" >
												<c:out value="02:情報管理" />
											</c:when>
											<c:when test="${user.position == '3' }" >
												<c:out value="03:支店長" />
											</c:when>
											<c:when test="${user.position == '4' }" >
												<c:out value="04:社員" />
											</c:when>
										</c:choose>
										</td>
			                   		</tr>
								</form>
							</c:forEach>
							</table>
						</div>

			    <a href="./">戻る</a>
				</div>
			</c:if>
			<c:if test="${loginUser.position != 1}">
				<font color="red">
				<c:out value="閲覧権限がありません"/>
				</font>
			</c:if>
			<br />
			<div class="copylight"> Copyright(c)ShinchiKasumi</div>
		</div>
	</body>
</html>