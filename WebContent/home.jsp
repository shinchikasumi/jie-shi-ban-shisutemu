<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>【HOME】</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">

		<script>
		function clickEvent() {
			alert("閲覧権限がありません");
		}
		</script>

	</head>
	<body>
		<div class="main-contents">
			<c:if test="${ not empty errorComments }">
                <div class="errorComments">
                    <ul>
                        <c:forEach items="${errorComments}" var="comment">
                            <li><c:out value="${comment}" />
                        </c:forEach>
                    </ul>
	                </div>
                <c:remove var="errorComments" scope="session" />
            </c:if>
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                     <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                	</ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>

			<div class="header">
				<c:if test="${ empty loginUser }">
					<a href="login">ログイン</a>してください
					<h3>はなまる掲示板</h3>
				</c:if>

				<c:if test="${ not empty loginUser }">
					<a href="./">ホーム</a>
					<a href="newpost">新規投稿</a>
					<c:choose>
					<c:when test="${ loginUser.position == 1 && loginUser.officeName == 1 }" >
					<a href="settings">管理者画面</a>
					</c:when>
					<c:when test="${ loginUser.position != 1 || loginUser.officeName != 1  }" >
					<a href="./" onclick="clickEvent()">管理者画面</a>
					</c:when>
					</c:choose>
					<a href="logout">ログアウト</a>
				    <br /><br />

					<!-- <form action="" method=""> -->
					【表示絞込み】<br />
					<form action="./" method="get">
					カテゴリー<input type="search" name ="search" list="list"><br />
					投稿日<input type="date" autocomplete="on" name ="date" list="list"> ～ <input type="date" autocomplete="on" name ="last_date" list="list"><br />
					<input type="submit" value="検索">
					</form>
					<br /><br />
				</c:if>
			</div>

			<c:if test="${ not empty loginUser }">
			    <div class="profile">
			        <div class="userName"><h2><c:out value="${loginUser.userName}" /></h2></div>
			        <div class="loginUser">
			            @<c:out value="${loginUser.account}" />
			        </div>
			        <div class="text">
			            <c:out value="${loginUser.text}" />
			        </div>
			    </div>
            <br />

			<div class="messages">
			 <c:forEach items="${messages}" var="message">
			    	<c:if test="${message.isDelete == 0}">
			            <div class="message">
			                <div class="id" style="float:left;"><c:out value="${message.id}:" /></div>
			                <div class="title">【件名】<c:out value="${message.title}" /></div>
			                <div class="date"><fmt:formatDate value="${message.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
			                <div class="userName">
			                	<span class="name"><c:out value="${message.userName}" /></span>
			                    <span class="account">@<c:out value="${message.account}" /></span>
			                </div>
			                <div class="text">【本文】<c:out value="${message.text}" /></div>
			                <div class="category">【カテゴリー】<c:out value="${message.category}" /></div>
						</div><br />
				        	<c:if test="${message.userId == loginUser.id}">
				        		<form action="messagedelete" method="post">
				        			<input name="isDelete" value="1" id="is_delete" type="hidden"/>
 									<input name="id" value="${message.id}" id="id" type="hidden"/>
				        			<input type="submit" value="削除"><br /><br />
								</form>
							</c:if>

			<div class="comments">
 				<c:if test="${ isShowCommentForm }">
					<c:forEach items="${userComment}" var="comment">
						<c:if test="${message.id == comment.messageId}">
							<c:if test="${comment.isDelete == 0}">
			            		<div class="comment">
			                		<span class="name">From:<c:out value="${comment.userName}" /></span>
			                   		<span class="account">@<c:out value="${comment.account}" /></span>
			                		<input name="id" value="${comment.messageId}" id="id" type="hidden"/>
			                		<div class="date"><fmt:formatDate value="${comment.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
			                		<div class="comment">【コメント】<c:out value="${comment.comment}" /></div>
				        		</div><br />
				        		<c:if test="${comment.userId == loginUser.id}">
				        			<form action="delete" method="post">
				        				<input name="isDelete" value="1" id="is_delete" type="hidden"/>
 										<input name="id" value="${comment.id}" id="id" type="hidden"/>
				        				<input type="submit" value="削除"><br /><br />
									</form>
								</c:if>
				        	</c:if>
						</c:if>
					</c:forEach>
 				</c:if>

					<c:if test="${ isShowMessageForm }">
				        <form action="comment" method="post">
					        <br />コメント（500文字以下）<br />
							<textarea name="comment" cols="100" rows="5" class="tweet-box" ></textarea><br />
							<input name="message_id" value="${message.id}" id="id" type="hidden"/>
							<input name="id" value="${loginUser.id}" id="id" type="hidden"/>
							<input type="submit" value="コメント" >
						</form>
					</c:if>
					<br />
				  <HR WIDTH="n" SIZE="3" color="pink" noshade>
				  <br /><br /></div>
				</c:if>
			   </c:forEach>
			</div></c:if><br />
		<div class="copylight"> Copyright(c)ShinchiKasumi</div>
	</div>
	</body>
</html>