<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>【新規投稿】</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">

		<script>
		function clickEvent() {
			alert("閲覧権限がありません");
		}
		</script>

	</head>
	<body>
		<div class="main-contents">
			<c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                 	<ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <input type="button" Value="戻る" onClick="history.go(-1);">

                <c:remove var="errorMessages" scope="session" />
			</c:if>

				<div class="header">
				<c:if test="${ empty loginUser }">
					<a href="login">ログイン</a>
				</c:if>
				<c:if test="${ not empty loginUser }">
					<a href="./">ホーム</a>
					<c:choose>
					<c:when test="${ loginUser.position == 1 && loginUser.officeName == 1 }" >
					<a href="settings">ユーザー管理</a>
					</c:when>
					<c:when test="${ loginUser.position != 1 || loginUser.officeName != 1 }" >
					<a href="./" onclick="clickEvent()">ユーザー管理</a>
					</c:when>
					</c:choose>
					<a href="logout">ログアウト</a>
				</div>
			    <div class="profile">
			        <div class="userName"><h2><c:out value="${loginUser.userName}" /></h2></div>
			        <div class="loginUser">
			            @<c:out value="${loginUser.account}" />
			        </div>
			        <div class="text">
			            <c:out value="${loginUser.text}" />
			       </div>
			    </div>
			    </c:if>


			<div class="form-area">
					<form action="newpost" method="post">
						件名（30文字以下）<br />
						<textarea name="title" cols="100" rows="5" class="tweet-title" ><c:out value ="${errorTitle}"/></textarea>
						<br />
						カテゴリ（10文字以下）<br />
						<textarea name="category" cols="100" rows="5" class="tweet-title" ><c:out value ="${errorCategory}"/></textarea>
						<br />
						本文（1000文字以下）<br />
						<textarea name="text" cols="100" rows="5" class="tweet-box" ><c:out value ="${errorText}"/></textarea>
						<br />
						<input type="submit" value="投稿"> <br /> <a href="./">戻る</a>
					</form>
			</div>
			<br />
			<div class="copylight"> Copyright(c)ShinchiKasumi</div>
		</div>
	</body>
</html>